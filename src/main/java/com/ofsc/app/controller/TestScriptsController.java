package com.ofsc.app.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ofsc.app.model.TestScripts;

@RestController
public class TestScriptsController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    /*@RequestMapping(method = RequestMethod.POST,value="/testscripts")
    public TestScripts runTestScripts(@RequestParam(value="name", defaultValue="World") String name) {
        return new TestScripts(counter.incrementAndGet(),
                            String.format(template, name));
        
        
    }*/
    
    @RequestMapping(method = RequestMethod.POST,value="/testscripts")
    public void runTestScripts(@RequestBody TestScripts testScripts) {
       System.out.println(testScripts);
        
    }
}
