package com.ofsc.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "pojo", description = "This model contains all test modules that will run.")

public class TestScripts {
    
	@ApiModelProperty(required=true,value="id",example="2")
	private  long id;
	
	@ApiModelProperty(required=true,value="content",example="Good")
    private String content="Good";

    public TestScripts(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
    
    @Override
   	public String toString() {
   		return "TestScripts [id=" + id + ", content=" + content + "]";
   	}
}
